# Recuperação simplificada de arquivos

Escrito e revisado por Allan Ribeiro

# Procedimento Operacional Padrão

### 0 

Desconecte qualquer outro dispositivo de armazenamento USB do computador, evitando que um dispositivo indesejado seja analisado por engano;

### 1

Baixe o [Testdisk](https://www.cgsecurity.org/testdisk-7.2-WIP.win.zip). Caso não tenha um programa para descrompressão de arquivos ".zip", recomendo o [7-zip](https://www.7-zip.org/a/7z2103-x64.exe);

### 2

Descompacte-o

### 3

Dentro da pasta do Testdisk, procure pelo binário do programa "testdisk_win.exe" e o execute. Por padrão, ele inicia no modo simplificado, que serve para a maioria dos usos.

### 4

Uma janela abrirá perguntando se você deseja registrar o procedimento[1], prefira a opção "Create"

### 5

Selecione o dispositivo a ser analizado[2]

### 6

Identifique sua tabela de partição[3]. Observe a recomendação no rodapé da tela;

### 7

Selecione "Advanced", e em seguida "Undelete" para dar início à leitura do dispositivo. Apesar de o programa não responder, a leitura está sendo feita. Isto pode demorar dependendo do tempo de uso do dispositivo

### 8

Você está dentro do seu disco, e pronto para buscar os arquivos. As instruções de manipulação estão no rodapé, mas:

	- Arquivos recuperáveis no dispositivo serão marcados na COR CINZA(4);

	- Arquivos SELECIONADOS e prontos para alguma operação serão marcados na COR VERDE;

	**Atente-se para a diferenciação entre letras maísculas e minúsculas**

	- Use SETAS para cima e baixo, para NAVEGAR um arquivo por vez;

	- Use PgUp e PgDown para pular para o início e para o final de uma PÁGINA de arquivos, respectivamente.

	- Para SELECIONAR um arquivo específico para ser copiado, pressione ":". Ele será marcado em VERDE para indicação.

	- Para selecionar TUDO de uma vez só, pressione "A" (shift+a);

	- Para COPIAR os arquivos SELECIONADOS, pressione "C" (shift+c);

		Você precisa agora selecionar um diretório onde salvar os arquivos. Vá para 9;

	- Para COPIAR apenas o ATUALMENTE SELECIONADO, pressione "c";

		Você precisa agora selecionar um diretório onde salvar os arquivos. Vá para 9;

	- Quando terminar, use "q" para sair;


### 9

A navegação das pastas é no mesmo padrão GNU/Linux: "." representa o diretório atual, ".." é uma "pasta" e representa o diretório pai: "C:\Arquivos de Programas" é o diretório pai de "C:\Arquivos de Programas\Windows NT". Use enter para adentrar uma pasta. Quando estiver no diretório desejado, pressione "C" (shift+c) para salvar os arquivos no diretório desejado, e repita os passos 8 e 9 para copiar mais arquivos.

	Neste ponto é recomendável criar um diretório no mesmo diretório do binário, e salvar os arquivos lá em vez de outra parte do computador.

### 10

Pressione "q" repetidamente para sair do programa;

[1] - Isso significa que todo e qualquer procedimento durante esta execução do programa será escrita num arquivo de texto, dentro do mesmo diretório do binário (testdisk.log). Esse arquivo pode ser usado para análise posterior mais minunciosa, mas conterá informação sensível sobre os dados em seu dispositivo.

[2] - O Testdisk funciona em tempo real. Isso significa que QUALQUER dispositivo de armazenamento conectado ao computador pode ser analisado, inclusive seu próprio HDD, sobre o qual seu sistema operacional executa. Certifique-se de selecionar o desejado, orientando-se pelo nome. Na duvida, você tambpem pode desconectar TODOS, identificar o nome do seu HDD interno, conectar o novo, e selecionar o desejado por exclusão.

[3] - Diferentes dispositivos podem ter diferentes arquiteturas de particionamento. Isso significa que selecionando uma arquitetura incorreta, os dados serão lidos incorretamente, e por consequência, pode ser que nenhuma informação extraída seja útil. No entanto, arquiteturas PERDIDAS podem ser recuperadas, ainda que a atual não seja compatível com a qual você selecionou. Exemplo: Eu formatei um HDD no Windows (utilizando "Formatar..." e "Formatação Rápida" em "Meu Computador"), então provavelmente a arquitetura será "Intel". Mas se anteriormente o disco foi um dia formatado num GNU/Linux, provavelmente tinha arquitetura "None". Na tentativa de recuperar informações da época em que ele era usado no GNU/Linux, eu deveria usar "None" como arquitetura.

[4] - Um arquivo "recuperável" não significa arquivo "saudável". O programa não tem como saber qual arquivo está ou não completamente saudável, uma vez que parte de seus bytes podem já terem sido sobrescritos. Ele identifica o header do arquivo e usa  a tabela de partição para identificar os antigos lugares dos bytes. Quanto mais tempo de uso, menor é a probabilidade dos bytes estarem juntos (que diminuiria o tempo de acesso), e não terem sido sobrescritos (permitindo que o arquivo seja recuperado por completo).

# Testdisk

## Página

[Home page](https://www.cgsecurity.org/wiki/TestDisk)

[Download](https://www.cgsecurity.org/testdisk-7.2-WIP.win.zip)

## Descrição

Testdisk é um software aberto e livre para recuperação de dados. Sua função principal é ajudar a recuperar partições perdidas e/ou recuperar setores de boot de discos, problemas geralmente provocados por falha em softwares (como virus, ou bugs), ou ação humana (acidentalmente apagar uma tabela de partição, ou parte de uma informação).

Dentre suas principais capacidades:

	Consertar tabela de partição, ou recuperar deletadas;
	Recuperar setores de boot FAT32 a partir de um backup;
	Reconstruir setores de boot FAT[12, 16, 32]
	Consertar tabelas FAT;
	Reconstruir setores de boot NTFS;
	Recuperar setores de boot NTFS a partir de um backup;
	Consertar MFT usando um mirror MFT;
	Localizar superblocos de backup ext[2, 3, 4];
	Recuperar arquivos em partições FAT, extFAT, NTFS e ext2;
	Copiar arquivos deletados em partições FAT, exFAT, NTFS e ext[2, 3, 4];

Testdisk é funcional tanto para usuários novatos quanto avançados. Para aqueles que sabem pouco, ou nada, sobre  técnicas de recuperação de dados, TestDisk pode ser usado para coletar informações sobre um dispositivo incapaz de boot que podem ser enviadas a um técnico para análise posterior. Para aqueles familiares com tais procedimentos, Testdisk será uma ferramenta útil para recuperação local.

# 7-zip

## Página

[Home page](https://www.7-zip.org/)

[Download](https://www.7-zip.org/a/7z2103-x64.exe)

## Descrição

7-Zip é um software aberto e livre para (des)compressão de arquivos. 

Dentre suas capacidades:

	Alta taxa de compressão no formato 7z com compressão LZMA e LZMA2;
	Suporte à compactação de descompactação: 7z, XZ, BZIP2, GZIP, TAR, ZIP and WIM;
	Somente descompactação: AR, ARJ, CAB, CHM, CPIO, CramFS, DMG, EXT, FAT, GPT, HFS, IHEX, ISO, LZH, LZMA, MBR, MSI, NSIS, NTFS, QCOW2, RAR, RPM, SquashFS, UDF, UEFI, VDI, VHD, VMDK, WIM, XAR and Z;
	Para formatos ZIP e GZIP, 7-Zip fornece uma taxa de compressão de 2-10%, sendo melhor que os implementados por PKZip e WinZip;
	Encriptação AES-256 extremamente forte para formatos 7z e ZIP;
	Capacidade de auto-extração para o formato 7z;
	Integração com o Windows Shell;
	Gerenciador próprio de arquivos;
	Suporte à linha de comando;
	Plugin para gerenciador FAR;
	Localização em 87 languagens;

Os resultados das taxas de compressão variam dependendo do tipo de informação comprimida. Normalmente, 7-Zip comprime para o formato 7z com cerca de 30-70% a mais de eficiência que o formato ZIP.
